Database Search (dbs) is an administrative module that allows admin to search 
for a particular text in title, fields, files and revision fields. It also 
allows the admin to search for certain path alias, pages containing certain 
taxonomy terms and users. 

It has been checked with MYSQL but since it uses db_select in most queries, 
it should be fine with other database management systems as well. 

To search, you can use the web interface by going to your_website_/dbs.
DBS also provides following drush commands:
- searchtitle(seti) : search the titles
- searchfields(sefi): search all fields
- searchfiles(sefile): search all files (useful especially if you want to 
delete unwanted files and you have too many files)
- searchtaxo(seta): search the pages in which a specific taxonomy term has 
been used.
- searchusers(seus): search all users
- searchalias(seal): search the path aliases. Input part of the path alias
- searchrevision(sere): Search revision fields.
- searchall(seall): Search title, fields, files, users, taxonomy


The same functionality can be found in the web interface as well. However, 
the web interface tries to reduce duplicates while the Drush interface gives 
out everything. This means that the web interface might not show ALL results 
(for example if a page contains the word twice (in body and section callout), 
it will simply give you that page once whereas in Drush, your results will 
show up twice. Drush tries to give you a node id whenever its possible so 
åååit shouldn't be a trouble finding those pages.

The web interface adds the wildcard “%” before and after the text that the 
admin submits. However, you need to specify wildcards while using Drush 
commands. In most cases, “%” before and after the text works fine.

DBS uses menu and access callback so that only people with appropriate 
permission can view the pages(/dbs/%/%/%).

Things to do:
1) make sure it works in all/most database systems
2) Results in web-interface (make it look better)

Help wanted!
Any kind of help would be appreciated!

Similar projects and how they are different
1) Search api: Search api consists of a large library and requires indexing. 
Therefore, you need to set up the server and the index. DBS doesn't have to 
set any kind of server or index. It accesses the data base directly and 
simply extracts the results. Also, you can search using Drush commands 
(this functionality is not offered by search api).
2)Scanner and replace scans the text fields of nodes and replaces them 
if specified by the admin. However, it's search is limited and often ends 
up missing out some fields. Furthermore, Scanner doesn't offer Drush 
commands and cannot search taxonomy terms.
