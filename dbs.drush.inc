<?php

/**
 * @file
 *
 * Provides Drush tasks for searching various database tables.
 *
 * Drush commands for the dbs.
 * Consists of following commands:
 * - searchtitle(seti) : search the titles
 * - searchfields(sefi): search all fields
 * - searchfiles(sefile): search all files (useful especially if you want to
 * delete unwanted files and you have too many files)
 * - searchtaxo(seta): search the pages in which a specific taxonomy term
 * Has been used.
 * - searchusers(seus): search all users
 * - searchalias(seal): search the path aliases. Input part of the path alias
 * - searchrevision(sere): Search revision fields.
 * - searchall(seall): Search title, fields, files, users, taxonomy
 */

/**
 * Implements hook_drush_command().
 */
function dbs_drush_command() {
  $items = array();
  $items['searchtitle'] = array(
    'description' => dt('Search node titles'),
    'aliases' => array('seti'),
    'arguments'   => array(
      'title' => 'Nodes with this title will be searched.',
      'sensitive' => 'Case sensitive? False = case-insensitive = default. Required IF you want to export the results to a file',
      'filename' => 'The name of the file (if you want to export the results to a file). INCLUDE EXTENSION. File will be saved in the outermost project folder (with sites, includes, modules folders). HOWEVER, if you input the filename, input sensitive value as well.Otherwise, the results will not be exported.',
    ),
    'options' => array(
      'title' => ' Title containng the searched string',
    ),
    'required-arguments' => 1,
  );
  $items['searchfields'] = array(
    'description' => dt('Search in fields (does not contain taxonomy terms, titles, etc.)'),
    'aliases' => array('sefi'),
    'arguments'   => array(
      'fields' => 'Nodes whose fields contain the searched string',
      'sensitive' => 'Case sensitive? False = case-insensitive = default. Required IF you want to export the results to a file',
      'filename' => 'The name of the file (if you want to export the results to a file). INCLUDE EXTENSION. File will be saved in the outermost Project folder (with sites, includes, modules folders). HOWEVER, if you input the filename, you should input sensitive value as well. Otherwise, the results will not be exported.',
    ),
    'required-arguments' => 1,
  );
  $items['searchfiles'] = array(
    'description' => dt('Search files'),
    'aliases' => array('sefile'),
    'arguments'   => array(
      'files' => 'entities containing files that contain the searched string',
      'filename' => 'The name of the file if you want to export the results to a file. INCLUDE EXTENSION. HOWEVER, if you input the filename, you should input sensitive value as well. Otherwise, the results will not be exported.',
    ),
    'required-arguments' => 1,
  );
  $items['searchtaxo'] = array(
    'description' => dt('Search taxonomy terms'),
    'aliases' => array('seta'),
    'arguments'   => array(
      'taxo' => 'Taxonomy term to search',
      'sensitive' => 'Case sensitive? False = case-insensitive = default. Required IF you want to export the results to a file',
      'filename' => 'The name of the file (if you want to export the results to a file). INCLUDE EXTENSION. However, if you input the filename, you should input sensitive value as well. Otherwise, the results will not be exported. HOWEVER, if you input the filename, you should input sensitive value as well. Otherwise, the results will not be exported.',
    ),
    'required-arguments' => 1,
  );
  $items['searchusers'] = array(
    'description' => dt('Search users'),
    'aliases' => array('seus'),
    'arguments'   => array(
      'users' => 'User to search',
      'filename' => 'The name of the file if you want to export the results to a file. INCLUDE EXTENSION. HOWEVER, if you input the filename, you should input sensitive value as well. Otherwise, the results will not be exported.',
    ),
    'required-arguments' => 1,
  );
  $items['searchalias'] = array(
    'description' => dt('Search path alias'),
    'aliases' => array('seal'),
    'arguments'   => array(
      'alias' => 'Path alias to search',
      'filename' => 'The name of the file if you want to export the results to a file. INCLUDE EXTENSION. HOWEVER, if you input the filename, you should input sensitive value as well. Otherwise, the results will not be exported.',
    ),
    'required-arguments' => 1,
  );
  $items['searchrevision'] = array(
    'description' => dt('Search revision fields '),
    'aliases' => array('sere'),
    'arguments'   => array(
      'revision' => 'Search results',
      'sensitive' => 'Case sensitive? FALSE = case-insensitive = default. Required IF you want to export the results to a file',
      'filename' => 'The name of the file (if you want to export the results to a file). INCLUDE EXTENSION. However, if you input the filename, you should input sensitive value as well. Otherwise, the results will not be exported. HOWEVER, if you input the filename, you should input sensitive value as well. Otherwise, the results will not be exported.',
    ),
    'required-arguments' => 1,
  );
  $items['searchall'] = array(
    'description' => dt('Search all '),
    'aliases' => array('seall'),
    'arguments'   => array(
      'all' => 'Search results',
      'sensitive' => 'Case sensitive? FALSE = case-insensitive = default. Required IF you want to export the results to a file',
      'filename' => 'The name of the file (if you want to export the results to a file). INCLUDE EXTENSION. However, if you input the filename, you should input sensitive value as well. Otherwise, the results will not be exported. HOWEVER, if you input the filename, you should input sensitive value as well. Otherwise, the results will not be exported.',
    ),
    'required-arguments' => 1,
  );
  return $items;
}

/**
 * Search among the node_titles.
 *
 * Param $title, $sensitive (Required if you are exporting the results to a
 * file, $file_to_export (to which it will be saved). Automatically considers
 * csv. Other formats of file cannot be created. The file destination is:
 * The main folder = the folder containing: Sites, includes etc.
 */
function drush_dbs_searchtitle() {
  $rows[] = array(
    dt('Id'),
    dt('Node title'),
    dt('Entity type'),
    dt('Bundle Type'),
  );

  $count = 0;
  $args = func_get_args();

  if (is_null($args)) {
    // Drush_print(dt('Please type a title name or a string containing wildcard
    // And atleast 3 characters'));
    drush_set_error('title', dt('No string was entered.'));
  }
  else {
    $title = $args[0];
    if (count($args) > 1) {
      $sensitive = $args[1];
    }
    else {
      $sensitive = 'FALSE';
    }
    $export_file = 'FALSE';
    if (count($args) > 2) {
      $export_file = 'TRUE';
      $file_to_export = 'titles_' . $args[2];
      $this_file = fopen($file_to_export, 'w');
      fputcsv($this_file, $rows[0]);
    }
    if ($sensitive == 'TRUE' || $sensitive == 'true') {
      $result = db_select('node', 'n')
        ->fields('n')
        ->where('title collate utf8_bin like :t', array(':t' => $title))
        ->execute();
      // If database = mysql, you want faster results and,
      // Are not concerned about security issues, use db_query,
      // $result = db_query('SELECT * from node where  title collate:
      // Utf8_bin like :t',
      // Array(':t'=>$title));
    }
    else {
      // Default = like, case insensitive.
      $result = db_select('node', 'n')
        ->fields('n')
        ->condition('title', $title, 'LIKE')
        ->execute();
    }
    foreach ($result as $node) {
      $row = array(
        $node->nid,
        $node->title,
        'node',
        $node->type,
      );
      if ($export_file == 'TRUE' || $export_file = 'true') {
        fputcsv($this_file, $row);
      }
      $rows[] = $row;
      $count++;
    }
    if ($export_file == 'TRUE' || $export_file == 'true') {
      fclose($this_file);
    }

    drush_print_table($rows);
    drush_print_r($count);
  }
}

/**
 * Search fields.
 *
 * Other formats of file cannot be created. The file destination is default:
 * the main folder = the folder containing sites, includes etc.
 */
function drush_dbs_searchfields() {
  $rows[] = array(
    dt('Id (nid by default)'),
    dt('nid value?'),
    dt('Entity type'),
    dt('Bundle Type'),
  );

  $count = 0;
  $args = func_get_args();

  if (is_null($args)) {
    drush_print(dt('Please type a field name or a string containing wildcard and
      atleast 3 characters'));
  }
  else {
    $stringfield = $args[0];

    $export_file = 'FALSE';
    if (count($args) > 1) {
      $sensitive = $args[1];
      if (count($args) > 2) {
        $export_file = 'TRUE';
        $file_to_export = 'fields_' . $args[2];
        $this_file = fopen($file_to_export, 'w');
        fputcsv($this_file, $rows[0]);
      }
    }
    else {
      $sensitive = 'FALSE';
    }
    // Keep track of duplicated nodes/fields.
    $redduplicate = array();
    // If database = mysql and not concerned about security, use db_query:
    // $tables=db_query('SELECT DISTINCT table_name, column_name
    // FROM information
    // _schema.columns where table_name like :fd', array(':fd'=>'field_data%'));
    $tables = db_select('information_schema.columns', 'isc')
          ->fields('isc', array('table_name', 'column_name'))
          ->condition('table_name', 'field_data%', 'LIKE')
          ->distinct()
          ->execute();
    foreach ($tables as $table) {
      if ($sensitive == 'TRUE' || $sensitive == 'true') {
        $eid = db_select($table->table_name, 't')
          ->fields('t')
          ->where(':cn collate utf8_bin like :fd', array(':cn' => $table->column_name, ':fd' => $stringfield))
          ->distinct()
          ->execute();
      }
      else {
        $eid = db_select($table->table_name, 't')
          ->fields('t')
          ->condition($table->column_name, $stringfield, 'LIKE')
          ->distinct()
          ->execute();
      }
      foreach ($eid as $un) {
        $temp_nid = NULL;

        if ($un->entity_type == 'node') {
          $temp_nid = $un->entity_id;
        }
        elseif ($un->entity_type == 'field_collection_item') {
          $temp_nid = dbs_get_nid($un->entity_type, $un->entity_id);
        }
        else {
          $temp_nid = NULL;
        }
        // Check if $temp_nid is not null and if the node has already been used.
        if (!is_null($temp_nid)) {
          $cnresult = dbs_check_nid($temp_nid, $redduplicate);
          // If the nid has not been listed previously.
          if ($cnresult[0] == 0) {
            // Put the entity_type and the $temp_nid in the array.
            $redduplicate[] = array($un->entity_type, $temp_nid);
            // $temp_node = node_load($temp_nid, NULL);
            $row = array(
              $temp_nid,
              'Yes',
              $un->entity_type,
              $un->bundle,
            );
          }
        }
        // If it is just drifting in the database somehow, list it.
        else {
          $row = array(
            $un->entity_id,
            'No',
            $un->entity_type,
            $un->bundle,
          );
        }
        if ($export_file == 'TRUE' || $export_file == 'true') {
          fputcsv($this_file, $row);
        }
        $rows[] = $row;
        $count++;
      }
    }
    drush_print_table($rows, TRUE);
    drush_print_r($count);
    if ($export_file == 'TRUE' || $export_file == 'true') {
      fclose($this_file);
    }
  }
}

/**
 * Check if the given node_id is present in the array.
 */
function dbs_check_nid($value, $array) {
  $length = count($array);
  if ($length > 0) {
    for ($i = 0; $i < $length; $i++) {
      $tem_array = $array[$i];
      if ($tem_array[1] == $value) {
        return array(1, $i);
      }
    }
  }
  return array(0, 0);
}

/**
 * Works on field_collection. If modified/deleted, return null.
 */
function dbs_get_nid($entity_type, $entity_id) {
  $model = entity_load_single($entity_type, $entity_id);

  if ($model == NULL) {
    /*
    * TODO : make displaying null field collection visible
    *
    * If you want the null field collections to be visible, :
    * drush_print_r("These field collections are null");
    * drush_print_r($entity_id); // drush_print_r($entity_type);
    */
    $temp_nid = NULL;
  }
  else {
    try{
      $xyz = $model->hostEntity();
      $temp_nid = $xyz->nid;
    }
    catch (Exception $e) {
      $temp_nid = NULL;
    }
  }
  return $temp_nid;
}

/**
 * Search files.
 *
 * Param filename(to search), filename(to save).
 */
function drush_dbs_searchfiles() {
  $rows[] = array(
    dt('Id of entity'),
    dt('NID?'),
    dt('Entity_type'),
    dt('Filename'),
    dt('Module'),
    dt('Count'),
    dt('Location'),
  );
  $count = 0;
  $args = func_get_args();

  if (is_null($args)) {
    drush_print(dt('Please type a field name or a string containing wildcard and atleast 3 characters'));
  }
  else {
    $stringfile = $args[0];
    $export_file = 'FALSE';
    if (count($args) > 1) {
      $export_file = 'TRUE';
      $file_to_export = 'files_' . $args[1];
      $this_file = fopen($file_to_export, 'w');
      fputcsv($this_file, $rows[0]);
    }
    /*
    * USE THIS DB_QUERY IF DB_SELECT DOESN'T WORK--
    * ON LARGE DATAS, DB_SELECT TAKES A VERY LONG TIME.
    * However, using db_query directly can be dangerous.
    */
    // $files = db_query('SELECT fm.filename, fm.fid, fm.uri, fus.type,
    // fus.id as usid, fus.count, fus.module, fus.fid FROM
    // file_managed fm left outer join file_usage fus on fm.fid =fus.fid
    // Where fm.filename like :sf ', array(':sf'=>$stringfile)); /
    $files = db_select('file_usage', 'fus')
      ->fields('fus', array('fid', 'type', 'count', 'module'))
      ->condition('filename', $stringfile, 'LIKE');
    $files->leftJoin('file_managed', 'fm', 'fm.fid = fus.fid');
    $files->addField('fus', 'id', 'usid');
    $files->addField('fm', 'filename', 'filename');
    $files->addField('fm', 'uri', 'uri');
    $files->distinct();
    $files = $files->execute();
    foreach ($files as $filei) {
      if ($filei->type == 'node') {
        $tn = node_load($filei->usid, NULL);
        $published = $tn->status;
        if (!$published) {
          $vv = ", unpublished";
        }
        else {
          $vv = '';
        }
        if (!is_null($tn)) {
          $row = array(
            $tn->nid,
            'yes' . $vv,
            'node',
            $filei->filename,
            $filei->module,
            $filei->count,
            drupal_dirname($filei->uri),
          );
        }
        // If you do not care about nodes that could not be loaded,
        // put the following else code in comments.
        else {
          $row = array(
            $filei->usid,
            'yes, but node could not be loaded',
            'node',
            $filei->filename,
            $filei->module,
            $filei->count,
            drupal_dirname($filei->uri),
          );
        }
      }
      elseif ($filei->type == 'field_collection_item') {
        $random = dbs_get_nid('field_collection_item', $filei->usid);
        // $tn = node_load($random, null);
        if (!is_null($random)) {
          $row = array(
            $random,
            'yes',
            $filei->type,
            $filei->filename,
            $filei->module,
            $filei->count,
            drupal_dirname($filei->uri),
          );
        }
        else {
          $row = array(
            $filei->usid,
            'could not not load node for this field collection',
            'field-collection',
            $filei->filename,
            $filei->module,
            $filei->count,
            drupal_dirname($filei->uri),
          );
        }
      }
      else {
        $row = array(
          $filei->usid,
          'no, ',
          $filei->type,
          $filei->filename,
          $filei->module,
          $filei->count,
          drupal_dirname($filei->uri),
        );
      }
      $rows[] = $row;
      $count++;
    }
    drush_print($export_file);
    if ($export_file == 'TRUE' || $export_file == 'true') {
      foreach ($rows as $row) {
        fputcsv($this_file, $row);
      }
    }
    drush_print_table($rows);
    drush_print_r($count);
    if ($export_file == 'TRUE' || $export_file == 'true') {
      fclose($this_file);
    }
  }
}

/**
 * Search taxonomy terms.
 *
 * Param taxonomy term, sensitive, filename(to save the results)
 * $taxonomy term is the term that we are searching.
 * If you want to export the results to an external file, specify the filename.
 * If you want to save the result to a file, you MUST specify sensitive.
 */
function drush_dbs_searchtaxo() {
  $rows[] = array(
    dt('Taxonomy term'),
    dt('tid'),
    dt('nid'),
  );
  $count = 0;
  $args = func_get_args();

  if (is_null($args)) {
    drush_print(dt('Please type a field name or a string containing wildcard and atleast 3 characters'));
  }
  else {
    $stringtaxo = $args[0];

    $export_file = 'FALSE';
    if (count($args) > 1) {
      $sensitive = $args[1];
      if (count($args) > 2) {
        $export_file = 'TRUE';
        $file_to_export = 'taxonomy_' . $args[2];
        $this_file = fopen($file_to_export, 'w');
        fputcsv($this_file, $rows[0]);
      }
    }
    else {
      $sensitive = 'FALSE';
    }
    if ($sensitive == 'TRUE' || $sensitive = 'true') {
      $terms_query = db_select('taxonomy_term_data', 'ttd')
        ->fields('ttd', array('name', 'tid'))
        ->where('name collate utf8_bin like :st', array(':st' => $stringtaxo));
      $terms_query->leftJoin('taxonomy_index', 'ee', 'ee.tid = ttd.tid');
      $terms_query->addField('ee', 'nid', 'nid');
      $terms = $terms_query->execute();
    }
    else {
      $terms_query = db_select('taxonomy_term_data', 'ttd')
                  ->fields('ttd', array('name', 'tid'))
                  ->condition('name', $stringtaxo, 'LIKE');
      $terms_query->leftJoin('taxonomy_index', 'ee', 'ee.tid = ttd.tid');
      $terms_query->addField('ee', 'nid', 'nid');
      $terms = $terms_query->execute();
    }
    foreach ($terms as $term) {
      $count++;
      $row = array(
        $term->name,
        $term->tid,
        $term->nid,
      );
      $rows[] = $row;
      if ($export_file == 'TRUE' || $export_file == 'true') {
        fputcsv($this_file, $row);
      }
    }
    drush_print_table($rows);
    drush_print_r($count);
  }
  if ($export_file == 'TRUE' || $export_file == 'true') {
    fclose($this_file);
  }
}

/**
 * Search user(names).
 *
 * Param string, filename (to save the results).
 * Specify the string that you want to search.
 * Followed by the filename if you want to export the results.
 */
function drush_dbs_searchusers() {
  $rows[] = array(
    dt('UID'),
    dt('Name'),
    dt('Role'),
  );
  $count = 0;
  $args = func_get_args();
  if (is_null($args)) {
    drush_print(dt('Please type a field name or a string containing wildcard and atleast 3 characters'));
  }
  else {
    $stringuser = $args[0];
    $export_file = 'FALSE';
    if (count($args) > 1) {
      $export_file = 'TRUE';
      $file_to_export = 'users_' . $args[1];
      $this_file = fopen($file_to_export, 'w');
      fputcsv($this_file, $rows[0]);
    }
    /*
    * If you want the search to be faster and are not concerned about security,
    * Use the following code instead of db_select.
    * $users_list=db_query('SELECT distinct u.uid, u.name, ur.rid, r.name as
      role_name FROM users u, role r, users_roles ur where u.uid=ur.uid  and
      u.name LIKE :su and ur.rid=r.rid', array(':su'=>$stringuser) );
    */
    $users_list = db_select('users', 'u')
      ->fields('u', array('uid', 'name'))
      ->condition('u.name', $stringuser, 'LIKE');
    $users_list->leftJoin('users_roles', 'ur', 'u.uid=ur.uid');
    $users_list->leftJoin('role', 'r', 'ur.rid=r.rid');
    $users_list->addField('r', 'name', 'role_name');
    $users_list = $users_list->execute();
    foreach ($users_list as $useri) {
      $row = array(
        $useri->uid,
        $useri->name,
        $useri->role_name,
      );
      if ($export_file == 'TRUE' || $export_file == 'true') {
        fputcsv($this_file, $row);
      }
      $count++;
      $rows[] = $row;
    }
    drush_print_table($rows);
  }
  drush_print_r($count);
  if ($export_file == 'TRUE' || $export_file == 'true') {
    fclose($this_file);
  }
}

/**
 * Search pathalias: Param string, filename(to save the results).
 */
function drush_dbs_searchalias() {
  $rows[] = array(
    dt('Path id'),
    dt('Alias for'),
    dt('Alias'),
  );
  $count = 0;
  $args = func_get_args();
  if (is_null($args)) {
    drush_print(dt('Please type a field name or a string containing wildcard and atleast 3 characters'));
  }
  else {
    $stringpath = $args[0];
    $export_file = 'FALSE';
    if (count($args) > 1) {
      $export_file = 'TRUE';
      $file_to_export = 'alias_' . $args[1];
      $this_file = fopen($file_to_export, 'w');
      fputcsv($this_file, $rows[0]);
    }
    /*
    * If you want the search to be faster and are not concerned about security,
      use the following code instead of db_select.
    * $paths=db_query('SELECT * FROM url_alias WHERE alias LIKE :al '
    */
    $paths = db_select('url_alias', 'ua')
       ->fields('ua')
       ->condition('alias', $stringpath, 'LIKE')
       ->execute();
    foreach ($paths as $pathed) {
      $row = array(
        $pathed->pid,
        $pathed->source,
        $pathed->alias,
      );
      if ($export_file == 'true' || $export_file == 'TRUE') {
        fputcsv($this_file, $row);
      }
      $rows[] = $row;
      $count++;
    }
    drush_print_table($rows);
    drush_print_r($count);
    if ($export_file == 'true' || $export_file == 'TRUE') {
      fclose($this_file);
    }
  }
}


/**
 * Search revision fields.
 *
 * Param string, sensitive, filename (to save the results)
 * If you want the file to be saved, you MUST specify the sensitive or not
 * Argument/param.
 */
function drush_dbs_searchrevision() {
  $count = 0;
  $rows[] = array(
    dt('Id of entity'),
    dt('Revision id'),
    dt('Nid?'),
    dt('Module'),
    dt('Bundle'),
  );
  $args = func_get_args();

  if (is_null($args)) {
    drush_print(dt('Please type a field name or a string containing wildcard and atleast 3 characters'));
  }
  else {
    $stringrevision = $args[0];
    $export_file = 'FALSE';
    if (count($args) > 1) {
      $sensitive = $args[1];
      if (count($args) > 2) {
        $export_file = 'TRUE';
        $file_to_export = 'revision_' . $args[2];
        $this_file = fopen($file_to_export, 'w');
        fputcsv($this_file, $rows[0]);
      }
    }
    else {
      $sensitive = 'FALSE';
    }
    // Keep track of duplicated nodes/fields
    $redduplicate = array();
    /*
    * If you want the search to be faster and are not concerned about security,
      use the following code instead of db_select:
    * $tables=db_query('SELECT DISTINCT table_name, column_name FROM
      information_schema.columns where table_name like :fd', array(':fd'=>'));
    */
    $tables = db_select('information_schema.columns', 'isc')
          ->fields('isc', array('table_name', 'column_name'))
          ->condition('table_name', 'field_revision%', 'LIKE')
          ->distinct()
          ->execute();
    foreach ($tables as $table) {
      // From each table, check every column which contains that text.
      if ($sensitive == 'TRUE' || $sensitive == 'true') {
        $eid = db_select($table->table_name, 't')
            ->fields('t')
            ->where(':cn collate utf8_bin like :fd', array(':cn' => $table->column_name, ':fd' => $stringrevision))
            ->distinct()
            ->execute();
      }
      else {
        $eid = db_select($table->table_name, 't')
          ->fields('t')
          ->condition($table->column_name, $stringrevision, 'LIKE')
          ->distinct()
          ->execute();
      }

      foreach ($eid as $un) {
        $count++;
        $temp_nid = NULL;
        if ($un->entity_type == 'node') {
          $temp_nid = $un->entity_id;
        }
        elseif ($un->entity_type == 'field_collection_item') {
          $temp_nid = dbs_get_nid($un->entity_type, $un->entity_id);
        }
        else {
          $temp_nid = NULL;
        }
        // Check if $temp_nid is not null and lies has been used previously.
        if (!is_null($temp_nid)) {
          $cnresult = dbs_check_nid($temp_nid, $redduplicate);
          // If the nid has not been listed previously,
          if ($cnresult[0] == 0) {
            // Put the entity_type and the $temp_nid in the array.
            $redduplicate[] = array($un->entity_type, $temp_nid);
            // $temp_node = node_load($temp_nid, NULL);
            $row = array(
              $temp_nid,
              $un->revision_id,
              'Yes',
              $un->entity_type,
              $un->bundle,
            );
            if ($export_file == 'TRUE' || $export_file == 'true') {
              fputcsv($this_file, $row);
            }
            $rows[] = $row;
          }
        }
        else {
          $row = array(
            $un->entity_id,
            $un->revision_id,
            'No',
            $un->entity_type,
            $un->bundle,
          );
          if ($export_file == 'TRUE' || $export_file == 'true') {
            fputcsv($this_file, $row);
          }
          $rows[] = $row;
        }
      }
    }
    if ($export_file == 'TRUE' || $export_file == 'true') {
      fclose($this_file);
    }
    drush_print_table($rows, TRUE);
    drush_print($count);
  }
}
/**
 * Search all which includes titles, fields, users, taxonomy terms, files.
 *
 * Param string, sensitive, filename
 * If you want to save the result to a field, you MUST specify sensitive
 */
function drush_dbs_searchall($args) {
  $args = func_get_args();
  if (is_null($args)) {
    drush_print(dt('Please type a field name or a string containing wildcard and atleast 3 characters'));
  }
  else {
    $stringall = $args[0];
    if (count($args) > 1) {
      $sensitive = $args[1];
    }
    else {
      $sensitive = 'FALSE';
    }
    if (count($args) > 2) {
      $file_to_export = $args[2];
      drush_dbs_searchtitle($stringall, $sensitive, $file_to_export);
      drush_print("-----------------------------------------------------");
      drush_dbs_searchfields($stringall, $sensitive, $file_to_export);
      drush_print("-----------------------------------------------------");
      drush_dbs_searchusers($stringall, $file_to_export);
      drush_print("-----------------------------------------------------");
      drush_dbs_searchtaxo($stringall, $sensitive, $file_to_export);
      drush_print("-----------------------------------------------------");
      drush_dbs_searchfiles($stringall, $file_to_export);
      drush_print("-----------------------------------------------------");
    }
    else {
      drush_dbs_searchtitle($stringall, $sensitive);
      drush_print("-----------------------------------------------------");
      drush_dbs_searchfields($stringall, $sensitive);
      drush_print("-----------------------------------------------------");
      drush_dbs_searchusers($stringall);
      drush_print("-----------------------------------------------------");
      drush_dbs_searchtaxo($stringall, $sensitive);
      drush_print("-----------------------------------------------------");
      drush_dbs_searchfiles($stringall);
      drush_print("-----------------------------------------------------");
    }
  }
}
