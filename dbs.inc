<?php
/**
 * @file
 * Contains forms for search.
 *
 * All other functions required to search the database.
 * Can search (in) the following:
 * - titles
 * - fields
 * - files
 * - taxonomy
 * - users
 * - alias
 * - revision fields.
 * - all(i.e., titles, fields, users. files, taxonomy terms)
 */

/**
 * Implements hook_form().
 */
function dbs_form($form, &$form_state) {
  // Search everything.
  $form['search_all'] = array(
    '#type' => 'textfield',
    '#title' => t('Search everything'),
    '#description' => t('Includes titles, fields, users, files, taxonomy terms.
     Does not include path alias and revision tables'),
    '#required' => FALSE,
  );
  // Search only titles.
  $form['search_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Search title field'),
    '#description' => t('Use this field if you are sure that the title contains
     the string. Only titles will be search.'),
    '#required' => FALSE,
  );
  // Search all fields.
  $form['search_fields'] = array(
    '#type' => 'textfield',
    '#title' => t('Search fields.'),
    '#description' => t('Gives you a list of nodes containing the string.
     This does not include taxonomy terms, files, users, roles, url aliases etc.'),
    '#required' => FALSE,
  );
  // Search all the files.
  $form['search_files'] = array(
    '#type' => 'textfield',
    '#title' => t('Search files. Only accepts file name (no file id)'),
    '#description' => t('Gives you a list of filenames with a link to the pages
     containing that file. For example, if you search "image", the result will
      be image.jpeg hyperlinked to the page using that file. If no page is
       using that file, it will show up with the name of the entity_type and
        id.'),
    '#required' => FALSE,
  );
  // Search all the taxonomy terms.
  $form['search_taxo'] = array(
    '#type' => 'textfield',
    '#title' => t('Search taxonomy terms.'),
    '#description' => t('Gives you a list of nodes using that taxonomy term'),
    '#required' => FALSE,
  );
  // Search users.
  $form['search_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Search user name.'),
    '#description' => t('Gives you a list of users with that username'),
    '#required' => FALSE,
  );
  // Search path aliases.
  $form['search_pathalias'] = array(
    '#type' => 'textfield',
    '#title' => t('Search path alias.'),
    '#description' => t('Gives you a list of path alias'),
    '#required' => FALSE,
  );
  // Search revision tables.
  $form['search_revision'] = array(
    '#type' => 'textfield',
    '#title' => t('Search revision fields.'),
    '#description' => t('Equivalent to searching revision fields. Does not
     include title search.'),
    '#required' => FALSE,
  );
  $form['submit_title'] = array(
    '#type' => 'submit',
    '#value' => 'Search',
  );
  return $form;
}

/**
 * Search page titles.
 */
function dbs_title($stringtitle) {
  $stringtitle = '%' . $stringtitle . '%';
  /*
  * If database = mysql, you want faster search and are not concerned with
  * security, use following code instead of db_Select.
  * $result=db_query('SELECT DISTINCT * FROM node where title like :st collate
  * utf8_general_ci', array(':st'=>$stringtitle));
  */
  $result = db_select('node', 'n')
    ->fields('n')
    ->condition('title', $stringtitle, 'LIKE')
    ->distinct()
    ->execute();
  $total = array();
  $count = 0;
  foreach ($result as $node) {
    $total[] = l($node->title, 'node/' . $node->nid, !empty(
      $node->comment_count) ? array(
        'attributes' => array(
          'title' => format_plural($node->comment_count, '1 comment',
          '@count comments'))) : array());
    $count++;
  }
  $num_rows = TRUE;
  $title = t("Results from titles, @c", array('@c' => $count));
  return $num_rows ? array(
    '#theme' => 'item_list__node',
    '#items' => $total,
    '#title' => $title,
  ) : FALSE;
}

/**
 * Search in the field_data.
 */
function dbs_fields($stringfield) {
  $stringfield = '%' . $stringfield . '%';
  $count = 0;
  $actual_count = 0;
  // Redduplicate keeps track of node ids that been listed already.
  $redduplicate = array();
  // $items_nodes keeps the node info so that they can be presented in the page.
  $items_nodes = array();
  $num_rows = FALSE;
  /*
  * If database = mysql, you want faster search and are not concerned with
  * security, use following code instead of db_Select.
  * $tables=db_query('SELECT DISTINCT table_name, column_name FROM
  * information_schema.columns where table_name like :fd',
  * array(':fd'=>'field_data%'));
  */
  $tables = db_select('information_schema.columns', 'isc')
    ->fields('isc', array('table_name', 'column_name'))
    ->condition('table_name', 'field_data%', 'LIKE')
    ->execute();
  foreach ($tables as $table) {
    // From each table, check every column which contains that text.
    $eid = db_select($table->table_name, 't')
        ->fields('t')
        ->condition($table->column_name, $stringfield, 'LIKE')
        ->distinct()
        ->execute();
    foreach ($eid as $un) {
      $temp_nid = NULL;
      if ($un->entity_type == 'node') {
        $temp_nid = $un->entity_id;
      }
      elseif ($un->entity_type == 'field_collection_item') {
        $temp_nid = dbs_get_nid($un->entity_type, $un->entity_id);
      }
      else {
        $temp_nid = NULL;
      }
      // Check if $temp_nid is not null and lies has been used previously.
      if (!is_null($temp_nid)) {
        $cnresult = dbs_check_nid($temp_nid, $redduplicate);
        if ($cnresult[0] == 0) {
          // Else, put the entity_type and the $temp_nid in the array.
          $redduplicate[] = array($un->entity_type, $temp_nid);
          $temp_node = node_load($temp_nid, NULL);
          $items_nodes[] = l($temp_node->title, 'node/' .
          $temp_node->nid, !empty($temp_node->comment_count) ? array(
            'attributes' => array(
              'title' => format_plural(
                $temp_node->comment_count, '1 comment',
                 '@count comments'))) : array());
        }
        $count++;
      }
      // Else, it is neither a node nor a field_collection. Don't list these.
      /*
      else {
        $items_nodes[] = $un->entity_type . ', entity_id: ' . $un->entity_id .
        ', bundle: ' . $un->bundle;
      }
      */
      $actual_count++;
    }
  }
  $num_rows = TRUE;
  $title = t('Results from fields @c', array('@c' => $count));
  return $num_rows ? array(
    '#theme' => 'item_list__node',
    '#items' => $items_nodes,
    '#title' => $title,
  ) : FALSE;
}

/**
 * Check if the given node_id is present in the array.
 */
function dbs_check_nid($value, $array) {
  $length = count($array);
  if ($length > 0) {
    for ($i = 0; $i < $length; $i++) {
      $tem_array = $array[$i];
      if ($tem_array[1] == $value) {
        // Return (1, index) if the value is found at index $i.
        return array(1, $i);
      }
    }
  }
  // Return (0,0) if the nid was not found.
  return array(0, 0);
}

/**
 * If a field collection entity is provided, return the nid containing it.
 */
function dbs_get_nid($entity_type, $tentity_id) {
  $model = entity_load_single($entity_type, $tentity_id);
  if ($model == NULL) {
    /*
    * TODO : make displaying null field collection visible
    * If you want the nodes which couldn't be loaded to show up,
    * Enable: drush_print_r("These field collections are null");
    * Drush_print_r($tentity_id);
    * Drush_print_r($entity_type);
    */
    $temp_nid = NULL;
  }
  else {
    try{
      $xyz = $model->hostEntity();
      $temp_nid = $xyz->nid;
    }
    catch (Exception $e) {
      $temp_nid = NULL;
    }
  }
  return $temp_nid;
}

/**
 * Search files.
 */
function dbs_files($stringfile) {
  $stringfile = '%' . $stringfile . '%';
  $items_files = array();
  $num_rows = FALSE;
  /*
  * Use db_query if database = mysql and no security concerns.
  * $files = db_query('SELECT fm.filename, fm.fid, fus.type, fus.id as usid,
  * fus.count FROM  file_usage fus left outer join file_managed fm on fm.fid =
  * fus.fid where fm.filename like :sf UNION (SELECT fm.filename, fm.fid,
  * fus.type, fus.id as usid, fus.count FROM file_managed fm left outer join
  * file_usage fus on fm.fid =fus.fid where fm.filename like :sf)', array
  * (':sf'=>$stringfile));
  */
  $files = db_select('file_usage', 'fus')
    ->fields('fus', array('fid', 'type', 'count', 'module'))
    ->condition('filename', $stringfile, 'LIKE');
  $files->leftJoin('file_managed', 'fm', 'fm.fid = fus.fid');
  $files->addField('fus', 'id', 'usid');
  $files->addField('fm', 'filename', 'filename');
  $files->addField('fm', 'uri', 'uri');
  $files->distinct();
  $files = $files->execute();
  $count = 0;
  foreach ($files as $filei) {
    if ($filei->type == 'node') {
      $tn = node_load($filei->usid, NULL);
      if (!is_null($tn)) {
        $items_files[] = l($filei->filename, 'node/' . $tn->nid,
        !empty($tn->comment_count) ? array(
          'attributes' => array(
            'title' => format_plural($tn->comment_count,
            '1 comment', '@count comments'))) : array());
        $count++;
      }
    }
    elseif ($filei->type == 'field_collection_item') {
      $tn = node_load(dbs_get_nid($filei->type, $filei->usid), NULL);
      if (!is_null($tn) && !is_null(dbs_get_nid($filei->type, $filei->usid))) {
        $items_files[] = l($filei->filename, 'node/' . $tn->nid, !empty(
          $tn->comment_count) ? array(
            'attributes' => array(
              'title' => format_plural($tn->comment_count, '1 comment',
              '@count comments'))) : array());
        $count++;
      }
      else {
        $items_files[] = $filei->filename . ', used by (module name): ' .
        $filei->type . ', file usage id: ' . $filei->usid;
      }
    }
    else {
      $items_files[] = $filei->filename . ', used by (module name): ' .
      $filei->type . ', file usage id: ' . $filei->usid;
    }
  }
  $num_rows = TRUE;
  $title = t('Results from files, @counted', array('@counted' => $count));
  return $num_rows ? array(
    '#theme' => 'item_list__node',
    '#items' => $items_files,
    '#title' => $title,
  ) : FALSE;
}


/**
 * Search taxonomy terms.
 */
function dbs_taxo($stringtaxo) {
  $stringtaxo = '%' . $stringtaxo . '%';
  $items_taxo = array();
  $num_rows = FALSE;
  $count = 0;
  $terms_query = db_select('taxonomy_term_data', 'ttd')
    ->fields('ttd', array('name', 'tid'))
    ->condition('name', $stringtaxo, 'LIKE');
  $terms_query->leftJoin('taxonomy_index', 'ee', 'ee.tid = ttd.tid');
  $terms_query->addField('ee', 'nid', 'nid');
  $terms = $terms_query->execute();
  foreach ($terms as $term) {
    $count++;
    $temp_node = node_load($term->nid, NULL);
    $items_taxo[] = l($temp_node->title, 'node/' . $temp_node->nid, !empty(
      $temp_node->comment_count) ? array(
        'attributes' => array(
          'title' => format_plural($temp_node->comment_count, '1 comment',
          '@count comments'))) : array());
  }
  $num_rows = TRUE;
  $title = t('Results: taxonomy terms, @c', array('@c' => $count));
  return $num_rows ? array(
    '#theme' => 'item_list__node',
    '#items' => $items_taxo,
    '#title' => $title,
  ) : FALSE;
}

/**
 * Search a user.
 */
function dbs_users($stringuser) {
  $stringuser = '%' . $stringuser . '%';
  $items_user = array();
  $count = 0;
  // Users list does not need to have role id etc information in this case.
  // Even though it is included in drush.
  $userslist = db_select('users', 'u')
    ->fields('u', array('uid', 'name'))
    ->condition('u.name', $stringuser, 'LIKE')
    ->execute();
  foreach ($userslist as $useri) {
    $items_user[] = l($useri->name, 'users/' . $useri->name);
    $count++;
  }
  $num_rows = TRUE;
  $title = t("Result from users, @c", array("@c" => $count));
  return $num_rows ? array(
    '#theme' => 'item_list__node',
    '#items' => $items_user,
    '#title' => $title,
  ) : FALSE;
}

/**
 * Search path aliases.
 */
function dbs_pathalias($stringpath) {
  $stringpath = '%' . $stringpath . '%';
  $item_alias = array();
  $count = 0;
  $paths = db_select('url_alias', 'ua')
    ->fields('ua')
    ->condition('alias', $stringpath, 'LIKE')
    ->execute();
  foreach ($paths as $pathed) {
    $count++;
    $item_alias[] = l($pathed->alias, $pathed->source);
  }
  $num_rows = TRUE;
  $title = t('Results: path alias, @c', array('@c' => $count));
  return $num_rows ? array(
    '#theme' => 'item_list__node',
    '#items' => $item_alias,
    '#title' => $title,
  ) : FALSE;
}

/**
 * Search revision fields.
 */
function dbs_revision($stringrevision) {
  $stringrevision = '%' . $stringrevision . '%';
  $count = 0;
  $actual_count = 0;
  $redduplicate = array();
  $items_nodes = array();
  $num_rows = FALSE;
  $tables = db_select('information_schema.columns', 'isc')
    ->fields('isc', array('table_name', 'column_name'))
    ->condition('table_name', 'field_revision%', 'LIKE')
    ->distinct()
    ->execute();
  foreach ($tables as $table) {
    $eid = db_select($table->table_name, 't')
      ->fields('t')
      ->condition($table->column_name, $stringrevision, 'LIKE')
      ->distinct()
      ->execute();
    foreach ($eid as $un) {
      $temp_nid = NULL;
      if ($un->entity_type == 'node') {
        $temp_nid = $un->entity_id;
      }
      elseif ($un->entity_type == 'field_collection_item') {
        $temp_nid = dbs_get_nid($un->entity_type, $un->entity_id);
      }
      else {
        $temp_nid = NULL;
      }
      if (!is_null($temp_nid)) {
        $cnresult = dbs_check_nid($temp_nid, $redduplicate);
        if ($cnresult[0] == 1) {
          $redduplicate[$cnresult[1]][0] = $redduplicate[$cnresult[1]][0] .
          ', ' . $un->entity_type;
        }
        elseif ($cnresult[0] == 0) {
          $redduplicate[] = array($un->entity_type, $temp_nid);
          $temp_node = node_load($temp_nid, NULL);
          $items_nodes[] = l($temp_node->title . ', ' .
            $un->revision_id, 'node/' . $temp_node->nid, !empty(
              $temp_node->comment_count) ? array(
                'attributes' => array(
                  'title' => format_plural($temp_node->comment_count,
                  '1 comment', '@count comments'))) : array());
        }
        $count++;
      }
      else {
        $items_nodes[] = $un->entity_type . ', entity_id: ' . $un->entity_id .
        ', bundle: ' . $un->bundle;
      }
      $actual_count++;
    }
  }
  // Drupal_set_message($count);
  // Drupal_set_message($actual_count);
  $num_rows = TRUE;
  $title = t('Results from revision fields, @c', array('@c' => $count));
  return $num_rows ? array(
    '#theme' => 'item_list__node',
    '#items' => $items_nodes,
    '#title' => $title,
  ) : FALSE;
}

/**
 * Search all fields: includes : titles, fields, files, taxonomy, users.
 */
function dbs_all($stringall) {
  $fromtitle = dbs_title($stringall);
  $fromfields = dbs_fields($stringall);
  $fromfiles = dbs_files($stringall);
  $fromtaxo = dbs_taxo($stringall);
  $fromusers = dbs_users($stringall);
  return array($fromtitle, $fromfields, $fromfiles, $fromtaxo, $fromusers);
}

/**
 * Implements hook_form_validate().
 */
function dbs_form_validate($form, &$form_state) {
  // If user doesn't submit anything, issue an error.
  if (is_null($form_state['values']['search_title']) &&
    is_null($form_state['values']['search_fields']) &&
    is_null($form_state['values']['search_files']) &&
    is_null($form_state['values']['search_taxo']) &&
    is_null($form_state['values']['search_user']) &&
    is_null($form_state['values']['search_pathalias'])  &&
    is_null($form_state['values']['search_revision']) &&
    is_null($form_state['values']['search_all'])) {
    form_set_error('value', t('Please enter a string to search'));
  }
}

/**
 * Implements hook_form_submit().
 */
function dbs_form_submit($form, &$form_state) {
  /*
  * It is important to put '/search' after all redirections because otherwise,
  * String such as *.jpg causes Drupal to attempt going to that URL.
  * @TODO: Find a better way to deal with this.
  */
  if (!($form_state['values']['search_all'] == '')) {
    $stringall = $form_state['values']['search_all'];
    $form_state['redirect'] = 'admin/dbs/all/' . $stringall . '/search';
  }
  elseif (!($form_state['values']['search_title'] == '')) {
    $stringtitle = $form_state['values']['search_title'];
    $form_state['redirect'] = 'admin/dbs/title/' . $stringtitle . '/search';
  }
  elseif (!($form_state['values']['search_fields'] == '')) {
    $stringfield = $form_state['values']['search_fields'];
    $form_state['redirect'] = 'admin/dbs/field/' . $stringfield . '/search';
  }
  elseif (!($form_state['values']['search_files'] == '')) {
    $stringfile = $form_state['values']['search_files'];
    $form_state['redirect'] = 'admin/dbs/file/' . $stringfile . '/search';
  }
  elseif (!($form_state['values']['search_taxo'] == '')) {
    $stringtaxo = $form_state['values']['search_taxo'];
    $form_state['redirect'] = 'admin/dbs/taxo/' . $stringtaxo . '/search';
  }
  elseif (!($form_state['values']['search_user'] == '')) {
    $stringuser = $form_state['values']['search_user'];
    $form_state['redirect'] = 'admin/dbs/user/' . $stringuser . '/search';
  }
  elseif (!($form_state['values']['search_pathalias'] == '')) {
    $stringalias = $form_state['values']['search_pathalias'];
    $form_state['redirect'] = 'admin/dbs/alias/' . $stringalias . '/search';
  }
  elseif (!($form_state['values']['search_revision'] == '')) {
    $stringrevision = $form_state['values']['search_revision'];
    $form_state['redirect'] = 'admin/dbs/revision/' . $stringrevision . '/search';
  }
}
